# OpenML dataset: Credit-Risk-Dataset

https://www.openml.org/d/43454

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Detailed data description of Credit Risk dataset:



Feature Name
Description




person_age
Age


person_income
Annual Income


personhomeownership
Home ownership


personemplength
Employment length (in years)


loan_intent
Loan intent


loan_grade
Loan grade


loan_amnt
Loan amount


loanintrate
Interest rate


loan_status
Loan status (0 is non default 1 is default)


loanpercentincome
Percent income


cbpersondefaultonfile
Historical default


cbpresoncredhistlength
Credit history length

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43454) of an [OpenML dataset](https://www.openml.org/d/43454). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43454/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43454/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43454/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

